const _ = require('lodash');
const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');

const passport = require('passport');
const passportJWT = require('passport-jwt');

const bcrypt = require('bcrypt-nodejs');

// JWT Start
const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;

const jwtOptions = {
  //https://github.com/themikenicholson/passport-jwt
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: 'putwhateveryouwanthere'
}

const strategy = new JwtStrategy(jwtOptions, (jwt_payload, next) => {
  console.log('payload recd', jwt_payload);
  //usually a database call:
  const user = users[_.findIndex(users, {
    id: jwt_payload.id
  })];
  if(user) {
    next(null, user);
  } else {
    next(null, false);
  }
});

passport.use(strategy);

// JWT End

const app = express();
app.use(passport.initialize());

//parse application/x-www-form-urlencoded
//for easier testing with postman or plain HTML forms
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

//refactor to pull from a db, etc...
const generateHash = password => {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

const validPassword = (user, password) => {
  return bcrypt.compareSync(password, user.password);
};

let users = [
  {
    id: 1,
    name: 'joe',
    password: generateHash('joe')
  },
  {
    id: 2,
    name: 'test',
    password: generateHash('test')
  }
];

//static route
app.use(express.static('public'));
app.get('/', (req, res) => {
  res.sendFile(process.cwd() + 'public/index.html');
});

//routes

app.get('/', (req, res) => {
  res.json({message:'Express is up'})
});


app.post('/login', (req, res) => {
  if(req.body.name && req.body.password) {
    const name = req.body.name;
    const password = req.body.password;

    //usually db call...
    const user = users[_.findIndex(users, { name })];
    if(!user) {
      res.status(401).json({message:'no such user found'});
    }
    if(validPassword(user, password)){
      const payload = {id:user.id};
      const token = jwt.sign(payload, jwtOptions.secretOrKey);
      res.json({message: 'ok', token});
    } else {
      res.status(401).json({message: 'invalid password'});
    }
  } else {
    res.json({message:'name and password required'});
  }
  
});

/*
Example usage - get a token, add a header of 
Authorization  Bearer <token>
*/
app.get('/secret', passport.authenticate('jwt', {
  session: false
}), (req, res) => {
  res.json("Success! You have a valid token!");
});


app.listen(3000, () => {
  console.log('Express running');
});
