//

const baseUri = 'http://localhost:3000';

showLoginForm = () => {
  const token = window.localStorage.getItem('token');
  const loginElement = document.getElementById('login');
  const logOffBtn = document.getElementById('logOffBtn');
  
  if(token) {
    loginElement.style.display = 'none';    
    logOffBtn.style.display = 'block';
  } else {
    logOffBtn.style.display = 'none';
    loginElement.style.display = 'block';
    
  }
};

logOff = () => {
  window.localStorage.removeItem('token');
  const tokenElement = document.getElementById('token');
  const resultElement = document.getElementById('result');
  resultElement.innerHTML = '';
  tokenElement.innerHTML = '';
  showLoginForm();
};

getToken = () => {
  const loginUrl = `${baseUri}/login`,
    xhr = new XMLHttpRequest();
    userElement = document.getElementById('username'),
    passwordElement = document.getElementById('password'),
    tokenElement = document.getElementById('token'),
    user = userElement.value,
    password = passwordElement.value;

  xhr.open('POST', loginUrl, true);
  xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
  xhr.addEventListener('load', (e) => {
    const responseObject = JSON.parse(e.currentTarget.response);
    console.log(responseObject);
    if(responseObject.token) {
      tokenElement.innerHTML = responseObject.token;
      window.localStorage.setItem('token', responseObject.token);
      showLoginForm();
    } else {
      tokenElement.innerHTML = 'No token received';
    }
  });
  
  const sendObject = JSON.stringify({name:user, password});
  console.log('going to send', sendObject);
  xhr.send(sendObject);
}

getSecret = () => {
  const secretUrl = `${baseUri}/secret`,
    xhr = new XMLHttpRequest(),
    token = window.localStorage.getItem('token'),
    resultElement = document.getElementById('result');

  xhr.open('GET', secretUrl, true);
  xhr.setRequestHeader('Authorization', `Bearer ${token}`);
  xhr.addEventListener('load', (e) => {
    let responseObject;    
    if(e.currentTarget.status !== 401)
      responseObject = JSON.parse(e.currentTarget.response);
    else 
      responseObject = e.currentTarget.responseText;

    resultElement.innerHTML = e.currentTarget.responseText;
  });

  xhr.send(null);
}